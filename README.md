# FoodClassifier

## Basic Usage
This project uses pipenv as a python package manager install it via 'sudo apt install pipenv'.  Alternatively use the provided requirements.txt if you prefer.

Install and run the application:
```
cd foodclassifier
pipenv install
pipenv shell
python src/pipeline.py
```
You can add new model architectures in models.py. Pass them to a Pipline class (in pipline.py main for example) to train and evaluate the model.