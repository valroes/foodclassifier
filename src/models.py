"""
model.py
========
Provides generator functions for different models. Each generator function should accept as input image height and with
and return the model.
"""
import tensorflow as tf
from tensorflow.keras.applications import ResNet50

def make_simple(img_height, img_width):
    model = tf.keras.Sequential(name="FoodClassifier")
    model.add(tf.keras.layers.Conv2D(
        filters=32,
        kernel_size=3,
        activation="relu",
        input_shape=(img_height, img_width, 3)
        ))
    model.add(tf.keras.layers.MaxPooling2D(pool_size=2, strides=2))
    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(units=128, activation="relu"))
    model.add(tf.keras.layers.Dense(units=1, activation="sigmoid"))

    return model

def make_resnet50(img_height, img_width):
    base_model = ResNet50(
        include_top=False,
        input_shape=(img_height, img_width, 3),
        weights="imagenet",
        pooling="max"
        )
    for layer in base_model.layers[:-26]:
        layer.trainable = False

    dense = tf.keras.layers.Dense(units=1, activation="sigmoid")(base_model.output)

    model = tf.keras.Model(
        inputs=base_model.input,
        outputs=dense
    )

    return model

def make_resnet50_alternative(img_height, img_width):
    model = ResNet50(
        include_top=False,
        input_shape=(img_height, img_width, 3),
        pooling="max")
    for layer in model.layers[:-26]:
        layer.trainable=False

    x=model.output
    pred=tf.keras.layers.Dense(1, activation="sigmoid")(x)
    return tf.keras.Model(inputs=model.input, outputs=pred)