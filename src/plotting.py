"""
plotting.py
===========
Provides plotting functionality for different training and evaluation metrics.
"""
import argparse
import joblib
import matplotlib.pyplot as plt

def roc(fpr, tpr, auc, dir):
    joblib.dump( (fpr, tpr, auc), f"{dir}/roc.joblib")
    plt.figure()
    plt.title("Receiver Operating Characteristic")
    plt.plot(
        fpr,
        tpr,
        color="red",
        lw=2,
        label=f"ROC curve (area = {auc})"
    )
    plt.plot([0 ,1],[0 ,1], color="blue", lw=2, linestyle="--")
    plt.xlim([0.0,1.0])
    plt.ylim([0.0,1.05])
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.legend(loc="lower right")
    plt.savefig(f"{dir}/roc.png")
    plt.close()

def loss_history(history, dir):

    plt.figure()
    plt.title("Validation Loss")
    plt.plot(history['val_loss'])
    plt.ylabel('loss value')
    plt.xlabel('No. epoch')
    plt.savefig(f"{dir}/validation_loss.png")
    plt.close()

    plt.figure()
    plt.title("Training Loss")
    plt.plot(history['loss'])
    plt.ylabel('loss value')
    plt.xlabel('No. epoch')
    plt.savefig(f"{dir}/training_loss.png")
    plt.close()

    plt.figure()
    plt.title("Loss")
    plt.plot(history['loss'], label='loss (training data')
    plt.plot(history['val_loss'], label='loss (validation data)')
    plt.ylabel('loss value')
    plt.xlabel('No. epoch')
    plt.legend(loc="best")
    plt.savefig(f"{dir}/loss.png")
    plt.close()

def accuracy_history(history, dir):
    plt.plot(history['binary_accuracy'], label='binary accuracy (training data)')
    plt.plot(history['val_binary_accuracy'], label='binary accuracy (validation data)')
    plt.ylabel('Accuracy value')
    plt.xlabel('No. epoch')
    plt.legend(loc="best")
    plt.savefig(f"{dir}/accuracy.png")
    plt.close()

def plot_file():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dir")
    args = parser.parse_args()
    h = joblib.load(f"{args.dir}/history.joblib")

    loss_history(h, args.dir)
    accuracy_history(h, args.dir)


if __name__ == "__main__":
    plot_file()
