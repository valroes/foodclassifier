"""
pipeline.py
===========
Provides pipelilne for training and evaluation models specified in models.py.
"""
import os
import json
import argparse
import joblib
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
#import matplotlib.pyplot as plt
import sklearn
from sklearn.metrics import accuracy_score, precision_score, recall_score

import models
import plotting

class Pipeline:
    def __init__(self, name, model_generator, learning_rate=0.01, batch_size=8, epochs=10, decay=5):
        self.name          = name
        self.model_gen     = model_generator
        self.batch_size    = batch_size
        self.learning_rate = learning_rate 
        self.epochs        = epochs
        self.decay         = decay
        self.img_height, self.img_width = self.img_dim = 240, 320

        self.train_gen = self.make_data_generator("data/TRAIN", augmentation=True)
        self.test_gen  = self.make_data_generator("data/TEST", augmentation=False)

        self.out_dir = f"./out/{self.name}"

        if not os.path.exists(self.out_dir):
            os.mkdir(self.out_dir)

        self.performance = dict()
        self.performance["hyper_parameters"] = {
            "batch_size": self.batch_size,
            "learning_rate": self.learning_rate,
            "epochs": self.epochs,
            "learning_decay": self.decay
            }


    def make_data_generator(self, dir, augmentation=False):
        if augmentation:
            datagen = ImageDataGenerator(
                featurewise_center=True,
                rescale=1/255.0,
                rotation_range=15
            )
        else:
            datagen = ImageDataGenerator()

        means = np.array([93, 112, 128])/255
        datagen.mean = means.reshape( (1,1,3) )
        dir_iterator = datagen.flow_from_directory(
            dir,
            target_size = self.img_dim,
            batch_size  = self.batch_size,
            class_mode  = "binary",
            shuffle     = augmentation
        )

        return dir_iterator

    def scheduler(self, epoch, learning_rate):
        """ Reduce the learning rate by a factor of 10 every 5 epochs. """
        if epoch > 0 and epoch % self.decay == 0:
            return learning_rate / 10 
        else:
            return learning_rate

    def load_model(self):
        self.model = self.model_gen(self.img_height, self.img_width)

        self.model.compile(
            optimizer=tf.keras.optimizers.Adam(learning_rate=self.learning_rate),
            loss="binary_crossentropy",
            metrics = [ tf.keras.metrics.BinaryAccuracy(
                name="binary_accuracy"
                )]
        )

    def load_weights(self):
        self.model.load_weights(f"{self.out_dir}/{self.name}.h5")

    def evaluate(self):
        gt = self.test_gen.labels
        pred = self.model.predict(
            self.test_gen,
            verbose=1
        )

        threshold = self.roc_curve(gt, pred)
        self.performance["threshold"] = float(threshold)

        pred_bin = np.where(pred > threshold, np.ones(pred.shape[1]), np.zeros(pred.shape[1]))

        self.performance["accuracy"]  = accuracy_score(gt, pred_bin)
        self.performance["precision"] = precision_score(gt, pred_bin)
        self.performance["recall"]    = recall_score(gt, pred_bin)

        print(self.performance)
        with open(f"{self.out_dir}/evaluation.json", "w") as f:
            json.dump(self.performance, f)

    def roc_curve(self, gt, pred):
        fpr, tpr, thresholds = sklearn.metrics.roc_curve(gt, pred)
        auc = round(sklearn.metrics.auc(fpr, tpr), 3)
        optimal_threshold = thresholds[np.argmax(tpr - fpr)]

        plotting.roc(fpr, tpr, auc, self.out_dir)
        self.performance["auc"] = auc

        return optimal_threshold

    def train(self):
        checkpoint = tf.keras.callbacks.ModelCheckpoint(
            f"{self.out_dir}/{self.name}.h5",
            save_best_only=True,
            save_weights_only=False,
            verbose=1,
            mode="auto"
            )

        steps=len(self.train_gen) #/self.batch_size

        lr_decay = tf.keras.callbacks.LearningRateScheduler(self.scheduler, verbose=1)

        h = self.model.fit(
            self.train_gen,
            validation_data=self.test_gen,
            steps_per_epoch=steps,
            epochs=self.epochs,
            batch_size=self.batch_size,
            callbacks=[checkpoint, lr_decay]
        )

        plotting.loss_history(h.history, self.out_dir)
        plotting.accuracy_history(h.history, self.out_dir)
        joblib.dump(h.history, f"{self.out_dir}/history.joblib")

def train_model(name, model, bs, lr, e, d):
    pipeline = Pipeline(
        name=name,
        model_generator=model,
        batch_size=bs,
        learning_rate=lr,
        epochs=e,
        decay=d
        )
    pipeline.load_model()
    #pipeline.load_weights()
    pipeline.train()
    pipeline.evaluate()

def main():
    train_model(
        "CustomModel",
        models.make_simple,
        32,
        0.01,
        10,
        5 
    )
    train_model(
        "ModifiedResNet",
        models.make_resnet50,
        256,
        0.01,
        12,
        30 
    )

if __name__ == "__main__":
    main()